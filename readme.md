# README

# About

This is a Python command-line script written to aid in finding possible
locations from an origin point, a set of waypoints, and other heuristic
factors.

# Requirements

You will need the Google Maps Python API package and a Google Maps API key to use this
script.

# Usage

Run ``python main.py --help`` for instructions on how to use this script.
