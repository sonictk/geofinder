#!/usr/bin/env python
"""This is the entry point of the program."""
import argparse
import csv
import logging
import lib
import os


def main():
    """The entry point."""
    parser = argparse.ArgumentParser(description='This program is a geofinder '
    'for searching locations along a given travel vector.')
    parser.add_argument('-v',
                        '--verbosity',
                        default='INFO',
                        help='The verbosity level of the logging.')
    parser.add_argument('-k',
                        '--googleAPIKey',
                        default='',
                        help='The Google API key to use for making requests.')
    parser.add_argument('-sp',
                        '--startingPoint',
                        required=True,
                        help='The starting location, given as an address.')
    parser.add_argument('-p',
                        '--point',
                        nargs='*',
                        action='append',
                        help='Any additional points that will be used. These '
                        'points should be specified also as addresses and in '
                        'sequence of travel.')
    parser.add_argument('-om',
                        '--omitLocations',
                        nargs='*',
                        default=None,
                        help='A space-separated list of addresses that will '
                        'not be considered as matching locations in the search.')
    parser.add_argument('-mt',
                        '--maxTime',
                        default=300,
                        type=float,
                        help='The maximum amount of time in minutes that the '
                        'path chosen is allowed to take.')
    parser.add_argument('-tm',
                        '--transitMode',
                        default='walking',
                        help='The method of transit.')
    parser.add_argument('-s',
                        '--speed',
                        default=5.0,
                        type=float,
                        help='The average travel speed. This should in km/h.')
    parser.add_argument('-pt',
                        '--placeTypes',
                        nargs='*',
                        default=None,
                        help='A space-separated list of types of places that '
                        'should be used in consideration. These need to be one '
                        'of the supported ``types`` as defined in the Google '
                        'Maps API.')
    parser.add_argument('-o',
                        '--output',
                        default=None,
                        help='The path to a file on disk that should have the '
                        'results written to.')
    args = parser.parse_args()

    logger = logging.getLogger(__name__)
    if args.verbosity == 'DEBUG':
        logger.setLevel(level=logging.DEBUG)
    elif args.verbosity == 'INFO':
        logger.setLevel(level=logging.INFO)
    elif args.verbosity == 'WARNING':
        logger.setLevel(level=logging.WARNING)
    elif args.verbosity == 'ERROR':
        logger.setLevel(level=logging.ERROR)
    elif args.verbosity == 'CRITICAL':
        logger.setLevel(level=logging.CRITICAL)

    logger.debug('Attempting to connect to Google using supplied API key...')
    client = lib.create_client(args.googleAPIKey)

    # Take the original destination and figure out a list of possible
    # destinations based on the cost factors, in order of importance for
    # consideration:
    #    * In the direction of the travel vector
    #    * The travel distance based on Google Maps directions for pedestrians
    #    * The terrain difficulty based on Google Maps elevation data
    #    * The total amount of time allowed for the journey
    #    * TODO: The estimated human activity around the destination and the travel
    #      route. (The less activity, the lower the cost) This is estimated
    #      based on the number of public photos there are available for that
    #      place in question.

    # NOTE: All units/calculations should be done in metric.
    geocodes = []
    origin = args.startingPoint
    origin_geocode = lib.get_geocode(client, origin)
    logger.debug('Determined {0}\'s geocode as: {1}'.format(origin, origin_geocode))
    geocodes.append(origin_geocode)

    pos_lat_count = 0
    pos_lng_count = 0
    lat_direction = 0
    lng_direction = 0

    if args.point:
        for i, pt in enumerate(args.point):
            geocode = lib.get_geocode(client, pt)
            logger.debug('Determined {0}\'s geocode as: {1}'.format(pt, geocode))
            geocodes.append(geocode)
            if i != 0:
                prev_pt = args.point[i-1]
                prev_pt_geocode = lib.get_geocode(client, prev_pt)
                if prev_pt_geocode[0] > geocode[0]:
                    pos_lat_count += 1
                else:
                    pos_lat_count -= 1
                if prev_pt_geocode[1] > geocode[1]:
                    pos_lng_count += 1
                else:
                    pos_lng_count -= 1

        # Determine direction to search in if multiple pts are specified
        if pos_lat_count > 0:
            lat_direction = 1
        elif pos_lat_count < 0:
            lat_direction = -1
        if pos_lng_count > 0:
            lng_direction = 1
        elif pos_lng_count < 0:
            lng_direction = -1
    logger.debug('Geocoords input: {0}'.format(geocodes))

    if len(geocodes) > 1:
        # Rough best-fit vector for the given sets of points
        best_fit_line = lib.find_best_fit_line(geocodes)
        logger.debug('Best-fit line determined as: {0}'.format(best_fit_line))
    else:
        best_fit_line = None

    max_distance = args.speed * args.maxTime / 60
    logger.debug('Retrieving possible locations within a maximum distance of: {0} km...'.format(max_distance))
    if args.placeTypes:
        logger.debug('Place types specified: {0}'.format(args.placeTypes))

    if args.omitLocations:
        omit_locations = args.omitLocations
    locations_in_range = lib.get_possible_locations(client=client,
                                                    origin=origin_geocode,
                                                    radius=max_distance * 1000,
                                                    place_types=args.placeTypes,
                                                    lat_direction=lat_direction,
                                                    lng_direction=lng_direction,
                                                    best_fit_line=best_fit_line,
                                                    omit_locations=omit_locations)

    logger.debug('Possible num. of locations: {0}'.format(len(locations_in_range)))
    # Sort locations by computed travel cost
    locations_in_range.sort(key=lambda x: x.travel_cost)
    for l in locations_in_range:
        logger.info('Score: {0} | Dist: {1} | Elev cost: {3} | Vec cost: {4} | Loc: {2}'.format(
            l.travel_cost, l.distance_away, l.name, l.elevation_cost,
            l.direction_cost))

    if args.output:
        if not os.path.isdir(os.path.dirname(args.output)):
            os.makedirs(os.path.dirname(args.output))
        if os.path.isfile(args.output):
            logger.warning('Removing existing file on disk: {0}'.format(args.output))
            os.remove(args.output)

        with open(args.output, 'wa') as csvfile:
            writer = csv.writer(csvfile, dialect='excel')
            writer.writerow(['Score',
                             'Location',
                             'Latitude',
                             'Longitude',
                             'Distance from origin (m)',
                             'Elevation cost (m)',
                             'Direction cost'])
            for l in locations_in_range:
                writer.writerow([round(l.travel_cost, 3),
                                 l.name,
                                 l.geocoords[0],
                                 l.geocoords[1],
                                 l.distance_away,
                                 round(l.elevation_cost, 3),
                                 l.direction_cost])
        logger.info('CSV written to: {0}'.format(args.output))


if __name__ == '__main__':
    logging.basicConfig(level=logging.ERROR)
    main()
