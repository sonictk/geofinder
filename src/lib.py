#!/usr/bin/env python
"""This module contains library functions that are useful for geofinding."""

import googlemaps
import itertools
import logging
from fuzzywuzzy import fuzz
from math import radians, sin, cos, sqrt, asin, fsum


def create_client(api_key):
    """This function returns a ``Client`` instance from Google."""
    return googlemaps.Client(key=api_key)


def get_geocode(client, address):
    """This returns the geographic coordinates of the given street address."""
    location = client.geocode(address=address,
                          components={'country' : 'Singapore'},
                          region='sg')[0]['geometry']['location']

    return (location['lat'], location['lng'])


def find_distance_between_locations(client, origin, destination, mode='walking'):
    """Finds the distance between two locations in metres."""
    result = client.distance_matrix(origins=origin,
                                  destinations=destination,
                                  mode=mode,
                                  units='metric',
                                  transit_routing_preference='less_walking')
    if result.get('rows'):
        # Fallback to using geocodes to get distance if Google API call fails
        try:
            return result['rows'][0]['elements'][0]['distance']['value']
        except (TypeError, KeyError):
            return find_distance_between_geocodes(origin, destination)


def find_distance_between_geocodes(first, second):
    """
    Finds the approx. distance in metres between two sets
    of geographical coordinates.
    """
    # Uses the Haversine Formula to determine the orthodromic distance
    R = 6372.8 # Earth's radius in km
    delta_longitude = radians(first[0] - second[0])
    delta_latitude = radians(first[1] - second[1])

    lat1 = radians(first[0])
    lat2 = radians(second[0])

    return R * 2000 * asin(sqrt(pow(sin(delta_latitude / 2), 2) + pow(cos(lat1) * cos(lat2) * sin(delta_longitude / 2), 2)))


def find_elevations_between_geocodes(client, first, second):
    """
    Finds the approx. elevation changes in metres between two sets of
    geographical coordinates.
    """
    # Determine the sampling rate by finding the distance between the two
    # geocodes first; sample every 100m
    sample_rate = find_distance_between_geocodes(first, second) / 100
    elevations = client.elevation_along_path(path=[first, second],
                                             samples=int(sample_rate))
    return elevations


def find_elevations_cost(elevations):
    """
    This function determines a heuristic cost of traversing the given
    ``list`` of ``elevations`` provided.
    """
    running_cost = 0.0
    for i, e in enumerate(elevations):
        if i == 0:
            continue
        previous_elevation = elevations[i-1].get('elevation')
        current_elevation = e.get('elevation')
        delta = abs(current_elevation - previous_elevation)
        # If leaving the water, it's considered easier
        if previous_elevation < 0 and current_elevation > 0:
            running_cost -= delta
        # If entering the water, it's considered harder
        elif previous_elevation > 0 and current_elevation < 0:
            running_cost += delta
        if current_elevation > previous_elevation:
            running_cost += delta
        elif current_elevation < previous_elevation:
            running_cost -= delta

    return running_cost


def find_best_fit_line(pts):
    """
    Finds the best-fit line given a set of x,y coords. Returns the x and y
    intercepts of the best-fit line.
    """
    # TODO: This math needs to be checked
    mean_x = fsum([p[0] for p in pts]) / len(pts)
    mean_y = fsum([p[1] for p in pts]) / len(pts)
    slope = fsum([(p[0] - mean_x) * (p[1] - mean_y) for p in pts]) / fsum([pow(p[0] - mean_x, 2) for p in pts])
    y_intercept = mean_y - (slope * mean_x)

    return (-y_intercept / slope, y_intercept)


class NearbyLocation(object):
    """This represents a location with reference to an original location."""

    @staticmethod
    def location_from_api_result(client,
                                 origin,
                                 api_result,
                                 lat_direction,
                                 lng_direction,
                                 best_fit_line):
        """
        This returns a ``NearbyLocation`` using a retrieved Google API
        result ``dict``.
        """
        name = api_result.get('name')
        place_id = api_result.get('place_id')
        location = (api_result['geometry']['location']['lat'],
                    api_result['geometry']['location']['lng'])
        if not name and not place_id:
            return

        elif not name and place_id:
            # Attempt to retrieve a name from the ID
            closest_location = client.reverse_geocode(latlng=place_id)[0]
            name = closest_location.get('formatted_address')
            place_type = closest_location.get('types')
            if not name or not place_type:
                return
        else:
            place_type = api_result.get('types')

        if name and location and place_id and place_type:
            location = NearbyLocation(client=client,
                                      origin=origin,
                                      name=name.encode('utf-8'),
                                      geocoords=location,
                                      place_type=place_type,
                                      place_id=place_id,
                                      lat_direction=lat_direction,
                                      lng_direction=lng_direction,
                                      best_fit_line=best_fit_line)


            return location

    def __init__(self,
                 client,
                 origin,
                 name,
                 geocoords,
                 place_type,
                 place_id,
                 lat_direction=0,
                 lng_direction=0,
                 best_fit_line=None):
        """The constructor."""
        self.client = client
        self.origin = origin
        self.name = name
        self.geocoords = geocoords
        self.place_type = place_type
        self.place_id = place_id

        # This is the distance in km between this location and the reference
        # origin.
        # TODO: Need to fix the locations function that uses gMaps API and have
        # it return consistent units.
        self.distance_away = find_distance_between_locations(
            client,
            self.origin,
            self.geocoords
        )

        self.elevations = find_elevations_between_geocodes(client,
                                                           self.origin,
                                                           self.geocoords)
        self.elevation_cost = find_elevations_cost(self.elevations)

        # TODO: Improve this rather than hardcoding an arbitrary cost to
        # locations that do not match the lat/long directions
        self.direction_cost = 0
        origin_lat = origin[0]
        origin_lng = origin[1]
        location_lat = self.geocoords[0]
        location_lng = self.geocoords[1]
        if (lat_direction == 1 and location_lat < origin_lat) or \
            (lat_direction == -1 and location_lat > origin_lat):
                self.direction_cost += 5000
        if (lng_direction == 1 and location_lng < origin_lng) or \
            (lng_direction == -1 and location_lng > origin_lng):
                self.direction_cost += 5000

        # This is the heuristically-determined cost of travel to this
        # location from the reference origin. It is an increasing score where
        # 0 determines no cost (i.e. no effort/resources required) and higher
        # scores indicate a greater cost.
        self.travel_cost = self.distance_away + self.elevation_cost + self.direction_cost


def get_possible_locations(client,
                           origin,
                           radius,
                           place_types=None,
                           lat_direction=0,
                           lng_direction=0,
                           best_fit_line=None,
                           omit_locations=None):
    """
    Finds possible locations near the ``origin`` that are within the
    specified ``radius``.

    :return: ``list`` of ``NearbyLocation`` instances.
    """
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    result = []
    if not omit_locations:
        omit_locations = []

    logger.debug('Locations to be omitted from search: '
            '{0}'.format(omit_locations))

    if place_types:
        api_result = []
        for p_type in place_types:
            api_result = client.places_radar(location=origin,
                                             radius=radius,
                                             type=p_type).get('results')
            if api_result:
                [result.append(
                    NearbyLocation.location_from_api_result(client,
                                                            origin,
                                                            r,
                                                            lat_direction,
                                                            lng_direction,
                                                            best_fit_line)
                    )
                    for r in api_result]
    else:
        api_result = client.places_nearby(location=origin,
                                          radius=radius).get('results')
        if api_result:
            [result.append(
                NearbyLocation.location_from_api_result(client,
                                                        origin,
                                                        r,
                                                        lat_direction,
                                                        lng_direction,
                                                        best_fit_line)
                )
                for r in api_result]

        if len(result) > 19:
            logger.warning('Google limits the number of places found to '
                    '20 if no keywords are specified!')

    # If locations are specified to be omitted, filter against those instead
    filtered_results = []
    if omit_locations:
        for r, location in itertools.product(result, omit_locations):
            fuzzy_score = fuzz.partial_ratio(r.name, location)
            # TODO: Maybe change this score to be heuristic based on no. of
            # words and length of the string being considered
            fuzzy_score_threshold = 90
            if r and fuzzy_score >= fuzzy_score_threshold:
                logger.debug('Omitting: {0} from results since it appears '
                'to match {1} with a fuzzy score of {2}!'\
                .format(r.name, location, fuzzy_score))
                filtered_results.append(r)

    return [r for r in result if r is not None and r not in filtered_results]
